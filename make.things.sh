#! /bin/bash

MINUSDAYS=0
while getopts 'm:' flag; do
  case "${flag}" in
    m) MINUSDAYS="${OPTARG}" ;;
    *) error "Unexpected option ${flag}" ;;
  esac
done
DATE=`date --date="${MINUSDAYS} days ago"`
YEAR=`date --date="${DATE}" +%Y`
MONTH=`date --date="${DATE}" +%-m`
DAY=`date --date="${DATE}" +%-d`
SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

dateSuffix () {
    if [ $1 -gt 10 ] && [ $1 -lt 14 ]; then
        SUFFIX='th'
    else
        RESULT=$(($1 % 10))
        case "$RESULT" in
            1) SUFFIX='st' ;;
            2) SUFFIX='nd' ;;
            3) SUFFIX='rd' ;;
            *) SUFFIX='th' ;;
        esac
    fi
    echo $SUFFIX
}

echo "Creating necessary files/folders for..."

# Check for year folder
if [ ! -d $SCRIPTDIR/$YEAR ]; then
    echo " - $YEAR"
    mkdir $SCRIPTDIR/$YEAR
fi
# Check for month folder
if [ ! -d $SCRIPTDIR/$YEAR/$MONTH ]; then
    echo " - `date --date="${DATE}" +%B`"
    mkdir $SCRIPTDIR/$YEAR/$MONTH
fi
# Check for Day file
if [ ! -f $SCRIPTDIR/$YEAR/$MONTH/`date --date="${DATE}" +%d`.md ]; then
    SUFFIX=$(dateSuffix $DAY)
    echo " - The $DAY$SUFFIX"
    touch $SCRIPTDIR/$YEAR/$MONTH/`date --date="${DATE}" +%d`.md

    # init file from template
    echo -e "# Things For `date --date="${DATE}" +%B` $DAY$SUFFIX, $YEAR\n\n## Things I Did Do\n\n* Thing One\n* Thing Two\n\n## Things I Didn't Do\n\n* Thing One\n* Thing Two\n\n## Things I Need To Do\n\n* Thing One\n* Thing Two" > $SCRIPTDIR/$YEAR/$MONTH/`date --date="${DATE}" +%d`.md
fi
